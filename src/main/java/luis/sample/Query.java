package luis.sample;


import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import luis.sample.dados.Funcionario;
import luis.sample.service.FuncionarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Classe relativa ao servico de consumo dos GraphQLs.
 * @author Luis
 */
@Component
public class Query implements GraphQLQueryResolver {

    @Autowired
    private FuncionarioService funcionarioService;

    public Optional<Funcionario> findFuncionarioByLogin(String login){
        return funcionarioService.findFuncionarioByLogin(login);
    }

}

