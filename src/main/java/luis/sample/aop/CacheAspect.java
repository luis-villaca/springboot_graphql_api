package luis.sample.aop;

import org.infinispan.client.hotrod.RemoteCache;
import org.infinispan.client.hotrod.RemoteCacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;


import luis.sample.dados.Funcionario;

import java.util.Optional;

@Component
@Aspect
public class CacheAspect {

    static long NUMBER_TIMEUNITS_TO_EXPIRE = 10;
    static TimeUnit TIME_UNIT = TimeUnit.SECONDS;

    private final RemoteCacheManager cacheManager;

    @Autowired
    public CacheAspect(RemoteCacheManager cacheManagerD) {
        this.cacheManager = cacheManagerD;
    }



    private static final Logger logger = LoggerFactory.getLogger(CacheAspect.class);


    @Around("execution(* luis.sample.repository.*.Funcionario*Repository.findByLogin(*))")
    public Object cacheEvent(final ProceedingJoinPoint pJP) throws Throwable {

        Object[] args = pJP.getArgs();

        String stringParameter = (String) args[0];
        logger.debug("EVENTO CAPTURADO:" + stringParameter);

        Object cachedFuncionario = pullFromCache(stringParameter);

        logger.debug("pulled:" + cachedFuncionario);

        if (cachedFuncionario == null) {
            Object optionalAnswer =  pJP.proceed();

            if (((Optional)optionalAnswer).isPresent()) {
                Object funcionario =  ((Optional) optionalAnswer).get();
                logger.debug("adding:" +funcionario);

                addToCache(stringParameter,funcionario);
            }

            return optionalAnswer;
        }
        return Optional.of(cachedFuncionario);
    }



    Object pullFromCache(String key) {
        RemoteCache<String, Object> cache = cacheManager.getCache("oidcapi");
        logger.debug("pulled instance:" + cache);
        return cache.get(key);
    }

    void addToCache(String key, Object funcionario) {
        // adds with login, email and ... keys
        RemoteCache<String, Object> cache = cacheManager.getCache("oidcapi");
        logger.debug("pulled instance:" + cache);
        cache.putAsync(key, funcionario, NUMBER_TIMEUNITS_TO_EXPIRE, TIME_UNIT);
    }
}