package luis.sample.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import luis.sample.dados.Funcionario;
import luis.sample.repository.FuncionarioRepository;
import luis.sample.repository.primary.FuncionarioPrimaryRepository;
import luis.sample.repository.secondary.FuncionarioSecondaryRepository;
import org.apache.commons.collections4.map.MultiValueMap;
import org.apache.commons.collections4.MultiMap;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Collection;

/**
 * @author Luis
 */
@Service
public class FuncionarioService {

    @Autowired
    private FuncionarioPrimaryRepository funcionarioPrimaryRepository;

    @Autowired
    private FuncionarioSecondaryRepository funcionarioSecondaryRepository;

    boolean isPrimary = true;
 
    private Object pullRepositoryRoundRobin() {
        if (isPrimary) {
            isPrimary = false;
            return funcionarioPrimaryRepository;
        } else {
            isPrimary = true;
            return funcionarioSecondaryRepository;
        }
    }

    public Optional<Funcionario> findFuncionarioByLogin(String login) {

       return ((FuncionarioRepository)pullRepositoryRoundRobin()).findByLogin(login);
    }



}



