sudo  docker pull jboss/infinispan-server:11.0


## one or more infinispan instances in the same server
# docker run -v $(pwd):/user-config -e CONFIG_PATH="/user-config/testing.xml"  -e USER=kWL0e35uC5 -e PASS=hDaRqtQP24 --name infini001 -t -p 11222:11222 jboss/infinispan-server:11.

# distributed instances accross different servers - bind to this server IP
sudo docker run  -v $(pwd)/infinispan:/user-config  --entrypoint "/opt/infinispan/bin/server.sh"  --name infini001 --network host -t -p 7800:7800 -p 11222:11222 jboss/infinispan-server:10.1 -c /user-config/jgroups.xml -b <this ip>